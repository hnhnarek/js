(function(){
    if (window.jQuery === undefined) {
        var done = false;
        var script = document.createElement("script");
        script.src = "https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js";
        script.onload = script.onreadystatechange = function(){
            if (!done && (!this.readyState || this.readyState == "loaded" || this.readyState == "complete")) {
                done = true;
                initMyBookmarklet();
            }
        };
        document.getElementsByTagName("head")[0].appendChild(script);
    } else {
        initMyBookmarklet();
    }

    function initMyBookmarklet() {
        (window.myBookmarklet = function() {
            prepareResources();
        })();
    }
})();


var jsons;
var index = 0;
var id = 0;

function prepareResources() {
    var head = document.getElementsByTagName('head')[0];
    var meta = document.createElement('meta');
    meta.setAttribute("http-equiv", "Access-Control-Allow-Origin");
    meta.setAttribute("content", "*");
    head.appendChild(meta);
    var body= document.getElementsByTagName('body')[0];
    frame= document.createElement('iframe'); /* global */
    body.appendChild(frame);

    getJsons();
}

function getJsons() {
    $.ajax({
        url: 'https://ext-prxoy.herokuapp.com/get_rows',
        type: 'GET',
        success: function (data) {
            var jsonObject = JSON.parse(data);
            jsons = jsonObject.rows;
            processJsons();
            /* sendEnrichedJsonBack(); */
        },
        error: function(err) {
            console.log(err);
        }
    });
}

function processJsons() {
    frame.src = jsons[index].value.linkedin_url.replace(/\/\/(\w+)./,'//www.');
    console.log("raised before" + index );
    frame.onload = function () {
        console.log("raised for" + index);
        enrichJson(frame.contentDocument.documentElement.outerHTML);
        var json = jsons[++index];
        if (json) {
            frame.src = json.value.linkedin_url;
        } else {
            index = 0;
            getJsons();
        }
    };
}

function enrichJson(htmlData) {
    console.log("raised enrich for" + index );
    var extractedData = parseHTML(htmlData);
    var json = {};
    json.bio = extractedData.skills;
    json.current_position = extractedData.title ? extractedData.title : extractedData.currentWorkingStatus;
    json.first_name = extractedData.firstName;
    json.last_name = extractedData.lastName;
    json.location = extractedData.location;
    json.picture_url = extractedData.imageURL;
    json.years_of_experience = extractedData.overallExperienceInMonths;
    json.groups = extractedData.groups;
    json.summary = extractedData.summary;
    json.summaryAsJson = extractedData.summaryAsJson;

    localStorage.setItem(id++, JSON.stringify(json));
}

function parseHTML(htmlData) {
    var htmlAsArray = $.parseHTML(htmlData);
    var view = htmlAsArray[htmlAsArray.length - 4]; /*maybe should be more strict condition to get the main view*/
    var profileElement = $(view).find("#profile");
    var experienceContainerElement = profileElement.find("#experience");
    var currentPositionDetailsElement = experienceContainerElement.find("[data-section='currentPositionsDetails']");
    var previousPositionsElements = $.makeArray(experienceContainerElement
        .find("[data-section='pastPositionsDetails']"));
    var educationContainerElement = profileElement.find("#education");


    var locationElement = profileElement.find("#demographics").find(".locality");
    var location = locationElement.text();

    var nameElement = profileElement.find("#name");
    var name = nameElement.text();

    var firstName = name.substr(0, name.indexOf(" "));
    var lastName = name.substr(name.indexOf(" ") + 1);

    var titleElement = profileElement.find("[data-section='headline']");
    var title = titleElement.text();

    var imageElement = profileElement.find("[data-section='picture']").find(".image");
    var imageURL = imageElement.data("delayed-url");

    var currentPositionElement = currentPositionDetailsElement.find(".item-title");
    var currentPosition = currentPositionElement.text();

    var currentWorkPlaceElement = currentPositionDetailsElement.find(".item-subtitle");
    var currentWorkPlace = currentWorkPlaceElement.text();
    var currentWorkingStatus = currentPosition + " " + currentWorkPlace;

    var skills = extractSkills(profileElement);

    var overallExperienceInMonths = extractOverallExperience(previousPositionsElements, currentPositionDetailsElement,
        educationContainerElement);

    var groups = extractGroups(profileElement);

    var summaryInfo = extractSummary(profileElement, previousPositionsElements, educationContainerElement);
    var summary = summaryInfo.summary;
    var summaryAsJson = summaryInfo.summaryAsJson;

    return {skills, title, currentWorkingStatus, firstName, lastName, location, imageURL, overallExperienceInMonths,
        groups, summary, summaryAsJson};

}

function getDurationInMonths(duration) {
    var parts = duration.split(" ");
    return (parts[0] ?
            (parts[0] * parts[1].includes("year") * 12 + parts[0] * parts[1].includes("month")) : 0) +
        (parts[2] ?
            (parts[2] * parts[3].includes("month")) : 0);
}

function extractSkills(profileElement, currentPositionDetailsElement, educationContainerElement) {
    var skillsContainerElement = profileElement.find("#skills");
    var skillsElements =  $.makeArray(skillsContainerElement.find(".skill"));
    var skillsList = skillsElements.map(function(skillElement) {
        return $(skillElement).text();
    });

    return skillsList.toString();
}

function extractOverallExperience(previousPositionsElements, currentPositionDetailsElement, educationContainerElement) {

    var previousPositionsDurations = previousPositionsElements.map(function(previousPositionElement) {
        return $(previousPositionElement).find(".date-range").text().match(/\((.*)\)/)[1];
    });

    var currentPositionsDateRangeElements = $.makeArray(currentPositionDetailsElement.find(".date-range"));
    var currentExperienceInMonths = currentPositionsDateRangeElements.reduce(function(summarizedRange, dateRangeElement) {
        var dateRange = $(dateRangeElement).text().match(/\((.*)\)/)[1];
        var rangeInMonths = getDurationInMonths(dateRange);
        return summarizedRange + rangeInMonths;
    }, 0);

    if(previousPositionsDurations && previousPositionsDurations.length) {
        var overallExperienceInMonths = previousPositionsDurations.reduce(function (prevValue, duration) {
            return prevValue + getDurationInMonths(duration);
        }, currentExperienceInMonths);
    } else {
        var educationRangeElements = $.makeArray(educationContainerElement.find(".date-range"));
        var lastEducationalYear = $($(educationRangeElements[0]).children()[1]).text();
        var firstCurrentWorkingYearElement = currentPositionDetailsElement.find(".date-range");
        var firstCurrentWorkingYear = $(firstCurrentWorkingYearElement.children()[0]).text().split(" ")[1];
        if(firstCurrentWorkingYear - lastEducationalYear > 3) {
            var overallExperienceInMonths = educationRangeElements.reduce(function(prevValue, element) {
                return prevValue + ($($(element).children()[1]).text() -  $($(element).children()[0]).text()) * 12;
            }, currentExperienceInMonths);
        }
    }

    return overallExperienceInMonths;
}

function extractSummary(profileElement, previousPositionsElements, educationContainerElement) {

    var summaryElement = profileElement.find("#summary").find(".description");
    var summary = summaryElement.text();
    var summaryAsJson = {};
    summaryAsJson.summary = summary;

    var previousPositionsInfo = previousPositionsElements.map(function(positionElement) {
        var title = $($(positionElement).find(".item-title")).text();
        var company = $($(positionElement).find(".item-subtitle")).text();
        var description = $($(positionElement).find(".description")).text();
        var range = $($(positionElement).find(".date-range")).text();
        return {title, company, description, range};
    });

    summary += previousPositionsInfo.reduce(function(info, positionInfo) {
        return info + " " + positionInfo.title + " " + positionInfo.company + " " + positionInfo.description + " " +  positionInfo.range;
    }, " ");


    summaryAsJson.positions = previousPositionsInfo;

    var educationElements = $.makeArray(educationContainerElement.find(".schools").children());
    var educationsInfo = educationElements.map(function(educationElement) {
        var degree = $($(educationElement).find(".item-title")).text();
        var university = $($(educationElement).find(".item-subtitle").find(".original")).text();
        var years = $($(educationElement).find(".date-range")).text();

        return {degree, university, years};
    });

    summary += educationsInfo.reduce(function(info, educationInfo) {
        return info + " " + educationInfo.degree + " " + educationInfo.university + " " + educationInfo.years;
    }, " ");

    summaryAsJson.educations = educationsInfo;


    return {summaryAsJson, summary};
}

function extractGroups(profileElement) {
    var groupsContainerElement = $(profileElement).find("#groups");
    var groupElements = $.makeArray($(groupsContainerElement).find(".group"));
    var groupNames = groupElements.map(function(group) {
        return $(group).text();
    });

    return groupNames;
}
